const mongoose = require("mongoose");

const HotelSchema = new mongoose.Schema({
  hotelName: {
    type: String,
    required: true,
    min: [3, "Hotel name should be more than 3 char"],
    max: [12, "Hotel name can not be more than 12 char"],
    unique: true
  },
}, {
    timestamps: true
});

module.exports = mongoose.model("Hotel", HotelSchema);
