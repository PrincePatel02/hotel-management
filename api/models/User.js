const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  id:{
    type: Number,
    required: true,
    unique: true
  },
  userName: {
    type: String,
    required: true,
    min: [3, "Username should be more than 3 char"],
    max: [12, "Username can not be more than 12 char"],
    unique: true
  },
  fname:{
    type: String,
    min: [3, "Username should be more than 3 char"],
    max: [12, "Username can not be more than 12 char"],
    required: true,
  },
  lname:{
    type: String,
    min: [3, "Username should be more than 3 char"],
    max: [12, "Username can not be more than 12 char"],
    required: true,
  },
  email:{
    type: String,
    required: true,
  },
  profile: {
    type:String,
    // contentType: String
  }
}, {
    timestamps: true
});

module.exports = mongoose.model("User", UserSchema);
