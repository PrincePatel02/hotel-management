const mongoose = require("mongoose");

const BookingDetailsSchema = new mongoose.Schema({
  customerName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  hotelName: {
    type: String,
    required: true
  },
  status:{
    type: String,
    required:true,
  },
  bookingId:{
    type: String,
  },
  checkInTime:{
    type:Date,
  },
  checkOutTime: {
    type:Date
  }
}, {
    timestamps: true
});

module.exports = mongoose.model("BookingDetails", BookingDetailsSchema);
