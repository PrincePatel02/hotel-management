const express = require("express");
const multer = require("multer");
const path = require("path");
const User = require("./models/User");
require("./db");
const app = express();
const port = 5000;

// Set up Multer for file uploads
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname, "uploads")); // Set your upload directory
  },
  filename: (req, file, cb) => {
    // Generate a unique filename, you can use a library like uuid
    const uniqueFileName = `${Date.now()}-${file.originalname}`;
    cb(null, uniqueFileName);
  },
});

const upload = multer({ storage });

app.get("/user-details")

app.post("/upload", upload.single("file"), async (req, res) => {
  const { id, userName, fname, lname, email } = req.body;
  const userExist = await User.findOne({id} && { email } && { userName });
  if (userExist)
    return res
      .status(401)
      .json({ error: "User with this email or userName already exist" });
      console.log(req.file);
  const newUser = new User({
    id,
    userName,
    fname,
    lname,
    email,
    profile: req.file.filename
    // {
    //   data: req.file.filename,
    //   contentType: "image/png",
    // },
  });
  // Save the data in database
  let savedUser = await newUser.save();
  return res.status(201).send(savedUser);
});

app.get("/get-user/:id", async (req, res) => {
  try {
    // console.log(typeof parseInt(req.params.id))
    const userDetails = await User.findOne({id: req.params.id});
    console.log(userDetails)
    res.json(userDetails);
  } catch (error) {
    console.error("Error fetching user details:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
